using System.Linq;
using LightInject;
using SlavooAngularTemplate.Repositories;

namespace SlavooAngularTemplate.DependencyInjection
{
    public class DataStoreCompositionRoot : ICompositionRoot
    {
        public void Compose(IServiceRegistry serviceRegistry)
        {
            if(serviceRegistry.AvailableServices.Any(svc=>svc.ServiceType == typeof(IDataStoreRepository))){
                return;
            }

            //todo a healthcheck that checks the vesion of the database

            //todo a database migration classes and support

            serviceRegistry.Register<IDataStoreRepository, DataStoreRepository>();
        }
    }
}