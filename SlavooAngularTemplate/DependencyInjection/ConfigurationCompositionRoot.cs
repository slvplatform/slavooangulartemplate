using System.IO;
using LightInject;
using Microsoft.Extensions.Configuration;

namespace SlavooAngularTemplate.DependencyInjection
{
    public class ConfigurationCompositionRoot : ICompositionRoot
    {
        public void Compose(IServiceRegistry serviceRegistry)
        {
            serviceRegistry.Register<IConfiguration>(sf=>
                new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .AddJsonFile("appsettings.local.json", true)
                    .AddEnvironmentVariables()
                    .AddCommandLine(sf.GetInstance<string[]>("args"))
                    .Build()
                , new PerContainerLifetime());
        }
    }
}