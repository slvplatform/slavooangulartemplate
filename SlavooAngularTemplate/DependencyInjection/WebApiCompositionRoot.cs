using System.Threading.Tasks;
using App.Metrics.Health;
using LightInject;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using LightInject.Microsoft.AspNetCore.Hosting;
using Slavoo.AspNetCore.AppMetrics;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging.Configuration;
using Microsoft.Extensions.Logging;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System;
using Newtonsoft.Json;

namespace SlavooAngularTemplate.DependencyInjection
{
    public class WebApiCompositionRoot : ICompositionRoot
    {
        public void Compose(IServiceRegistry serviceRegistry)
        {
            //todo this needs more thinking. The assembly version may be important to determine if the version is correct
            var controllersWithRoutes = GetType().Assembly.GetTypes().Where(t=>t.GetCustomAttributes(false).OfType<RouteAttribute>().Any()).ToArray();

            var obsoleteRoutes = controllersWithRoutes.Where(r=>r.GetCustomAttributes(false).OfType<ObsoleteAttribute>().Any()).ToArray();

            var obsoleteVersions = obsoleteRoutes.Select(GetObsoleteRouteMessage)
                .Select(or=>$"{or.Version}: {or.Message.Replace(",","\\,").Replace(":","\\:")}");

            var supportedRoutes = controllersWithRoutes.Except(obsoleteRoutes).ToArray();

            var supportedVersions = supportedRoutes.Select(GetVersion).ToArray();

            var supportedVersionsString = string.Join(", ", supportedVersions);

            var obsoleteVersionsString = string.Join(", ", obsoleteVersions);


            serviceRegistry.RegisterInstance(
                new HealthCheck("SUPPORTED_VERSIONS", () => new ValueTask<HealthCheckResult>(HealthCheckResult.Healthy(supportedVersionsString)))
                , "supported_versions");

            if(obsoleteVersions.Any()){
                serviceRegistry.RegisterInstance(
                    new HealthCheck("DEPRECATED_VERSIONS", () => new ValueTask<HealthCheckResult>(HealthCheckResult.Healthy(obsoleteVersionsString)))
                    , "deprecated_versions");
            }
            
            serviceRegistry.Register(sf => {
                
                var healthChecks = sf.GetAllInstances<HealthCheck>();

                return new WebHostBuilder()
                .UseConfiguration(sf.GetInstance<IConfiguration>())
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureLogging((host, logger)=> logger.AddConfiguration(host.Configuration.GetSection("Logging")).AddConsole())
                .UseKestrel()
                .ConfigureServices(svc=>{
                    svc.AddSingleton<IServiceContainer>(sf.GetInstance<IServiceContainer>());
                    svc.AddSingleton<IConfiguration>(sf.GetInstance<IConfiguration>());
                })
                .WithSlavooInstrumentation(healthChecks)
                .UseStartup<Startup>();
            }
            , new PerContainerLifetime());

            serviceRegistry.Register<IWebHost>(
                sf=> sf.GetInstance<IWebHostBuilder>().Build()
                , new PerContainerLifetime());
        }

        private string GetVersion(Type controllerType)
        {
            var routeAttribute = controllerType.GetCustomAttributes(false).OfType<RouteAttribute>().Single();       

            return routeAttribute.Template.Split('/')[1]; 
        }

        private (string Version, string Message) GetObsoleteRouteMessage(Type controllerType)
        {
            var obsoleteAttribute = controllerType.GetCustomAttributes(false).OfType<ObsoleteAttribute>().Single();

            return (GetVersion(controllerType), obsoleteAttribute.Message);
        }
    }
}