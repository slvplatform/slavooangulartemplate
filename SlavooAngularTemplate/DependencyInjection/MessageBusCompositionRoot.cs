using LightInject;

namespace SlavooAngularTemplate.DependencyInjection
{
    public class MessageBusCompositionRoot : ICompositionRoot
    {
        public void Compose(IServiceRegistry serviceRegistry)
        {
            //todo implement message bus configuration
        }
    }
}