using System.Threading.Tasks;
using LightInject;
using Microsoft.AspNetCore.Hosting;

namespace SlavooAngularTemplate
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var container = BuildContainer(args);
            var webHost = container.GetInstance<IWebHost>();
            await webHost.StartAsync();
            await webHost.WaitForShutdownAsync();
        }

        private static IServiceContainer BuildContainer(string[] args){

            var container = new ServiceContainer(new ContainerOptions{
                EnablePropertyInjection = false,
                EnableVariance = false
            });
            
            container.RegisterInstance<IServiceContainer>(container);
            container.RegisterInstance(args,"args");
            container.RegisterAssembly(typeof(Program).Assembly);

            return container;
        }
    }
}