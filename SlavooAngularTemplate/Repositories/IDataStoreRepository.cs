namespace SlavooAngularTemplate.Repositories
{
    public interface IDataStoreRepository
    {
         int GetValue();
    }
}