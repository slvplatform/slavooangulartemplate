using System;

namespace SlavooAngularTemplate.Repositories
{
    public class DataStoreRepository: IDataStoreRepository
    {
        private readonly Random _random = new Random(); 
        public int GetValue(){
            return _random.Next();
        }
    }
}