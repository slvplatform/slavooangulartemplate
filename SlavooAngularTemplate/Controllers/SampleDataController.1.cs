using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SlavooAngularTemplate.Repositories;

namespace SlavooAngularTemplate.Controllers
{
    [Route("api/v1/[controller]")]
    [Obsolete("use v2 instead. This version will be removed on ...")]
    public class OldSampleDataController : Controller
    {
    }
}
