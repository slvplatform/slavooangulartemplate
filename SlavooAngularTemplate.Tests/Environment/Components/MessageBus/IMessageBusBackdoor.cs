using System.Threading;
using System.Threading.Tasks;

namespace SlavooAngularTemplate.Tests.Environment.Components.MessageBus
{
    public interface IMessageBusBackdoor
    {
        Task Start(CancellationToken token = default(CancellationToken));

        Task Stop(CancellationToken token = default(CancellationToken));
    }
}